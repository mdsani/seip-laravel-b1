<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\UnitController;
use App\Http\Controllers\WelcomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('welcome');
});

// Route::get('/home', function(){
//     return view('backend.home');
// });

Route::get('/home', function(){
    return view('backend.home');
});









Route::get('categories/export/', [CategoryController::class, 'export'])->name('categories.export');

Route::get('categories/pdf/', [CategoryController::class, 'downloadPdf'])->name('categories.pdf');

Route::get('/trashed-categories', [CategoryController::class, 'trash'])->name('categories.trashed');

Route::get('/trashed-categories/{category}/restore', [CategoryController::class, 'restore'])->name('categories.restore');

Route::delete('/trashed-categories/{category}/delete', [CategoryController::class, 'delete'])->name('categories.delete');

Route::resource('categories', CategoryController::class);


// Route::get('/categories/{category}', [CategoryController::class, 'show'])->name('categories.show');


// Route::get('/categories', [CategoryController::class, 'index'])->name('categories.index');

// Route::get('/categories/create', [CategoryController::class, 'create'])->name('categories.create');

// Route::post('/categories', [CategoryController::class, 'store'])->name('categories.store');

// // GET	/photos/{photo}	show	photos.show
// Route::get('/categories/{category}', [CategoryController::class, 'show'])->name('categories.show');

// // GET	/photos/{photo}/edit	edit	photos.edit
// Route::get('/categories/{category}/edit', [CategoryController::class, 'edit'])->name('categories.edit');

// // PUT/PATCH	/photos/{photo} update	photos.update
// Route::patch('/categories/{category}', [CategoryController::class, 'update'])->name('categories.update');

// // DELETE	/photos/{photo}	destroy	photos.destroy
// Route::delete('/categories/{category}', [CategoryController::class, 'destroy'])->name('categories.destroy');




Route::get('/tags', [TagController::class , 'index'])->name('tags.index');

Route::get('/tags/create', [TagController::class , 'create'])->name('tags.create');

Route::post('/tags', [TagController::class , 'store'])->name('tags.store');


// GET	/photos/{photo}	show	photos.show
Route::get('/tags/{tag}', [TagController::class , 'show'])->name('tags.show');

// GET	/photos/{photo}/edit	edit	photos.edit
Route::get('/tags/{tag}/edit', [TagController::class , 'edit'])->name('tags.edit');

// PUT/PATCH	/photos/{photo} update	photos.update
Route::patch('/tags/{tag}', [TagController::class , 'update'])->name('tags.update');

// DELETE	/photos/{photo}	destroy	photos.destroy
Route::delete('/tags/{tag}', [TagController::class , 'destroy'])->name('tags.destroy');

Route::get('/tag-trash', [TagController::class , 'trash'])->name('tags.trash');

Route::get('/tag-trash/{tag}/restore', [TagController::class , 'restore'])->name('tags.restore');

Route::get('/tag-trash/{tag}/permanentdelete', [TagController::class , 'permanentdelete'])->name('tags.permanentdelete');

Route::get('/tag-pdf', [TagController::class , 'pdf'])->name('tags.pdf');

Route::get('/tag-excel', [TagController::class , 'excel'])->name('tags.excel');





Route::resource('units', UnitController::class);

// Route::get('/units', [UnitController::class, 'index'])->name('units.index');

// Route::get('/units/create', [UnitController::class, 'create'])->name('units.create');

// Route::post('/units', [UnitController::class, 'store'])->name('units.store');

// // GET	/photos/{photo}	show	photos.show
// Route::get('/units/{unit}', [UnitController::class, 'show'])->name('units.show');

// // GET	/photos/{photo}/edit	edit	photos.edit
// Route::get('/units/{unit}/edit', [UnitController::class, 'edit'])->name('units.edit');

// // PUT/PATCH	/photos/{photo} update	photos.update
// Route::patch('/units/{unit}', [UnitController::class, 'update'])->name('units.update');

// // DELETE	/photos/{photo}	destroy	photos.destroy
// Route::delete('/units/{unit}', [UnitController::class, 'destroy'])->name('units.destroy');