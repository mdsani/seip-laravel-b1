<x-backend.layouts.master>
    <x-slot name="pageTitle">
        Tags
    </x-slot>

    <x-slot name='breadCrumb'>
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader"> Tags </x-slot>

            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">Tags</li>

        </x-backend.layouts.elements.breadcrumb>
    </x-slot>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Tags 
            <a class="btn btn-sm btn-info" href="{{ route('tags.create') }}">Add New</a>
            <a class="btn btn-sm btn-danger" href="{{ route('tags.trash') }}">Trash</a>
            <a class="btn btn-sm btn-success" href="{{ route('tags.excel') }}">Excel</a>
            <a class="btn btn-sm btn-success" href="{{ route('tags.pdf') }}">Pdf</a>
        </div>
        <div class="card-body">

            <x-backend.layouts.elements.message :message="session('message')" />

            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>SL#</th>
                        <th>Title</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $sl = 0;
                    @endphp
                    @foreach ($tag as $tag)
                        <tr>
                            <td>{{ ++$sl }}</td>
                            <td>{{ $tag->title }}</td>
                            <td>
                                <a class="btn btn-info btn-sm" href="{{ route('tags.show', ['tag'=>$tag->id]) }}" >Show</a>
    
                                <a class="btn btn-warning btn-sm" href="{{ route('tags.edit', ['tag'=>$tag->id]) }}" >Edit</a>
    
                                <form style="display:inline" action="{{ route('tags.destroy', ['tag'=>$tag->id]) }}" method="post">
                                    @csrf
                                    @method('delete')
                                    
                                    <button onclick="return confirm('Are you sure want to delete ?')" class="btn btn-sm btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</x-backend.layouts.master>