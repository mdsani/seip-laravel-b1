
            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>SL#</th>
                        <th>Title</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $sl = 0;
                    @endphp
                    @foreach ($tags as $tag)
                        <tr>
                            <td>{{ ++$sl }}</td>
                            <td>{{ $tag->title }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>