<x-backend.layouts.master>
    <x-slot name="pageTitle">
        Tags
    </x-slot>

    <x-slot name='breadCrumb'>
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader"> Tags </x-slot>

            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">Tags Trash</li>

        </x-backend.layouts.elements.breadcrumb>
    </x-slot>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Tags 
            <a class="btn btn-sm btn-info" href="{{ route('tags.index') }}">List</a>
            <a class="btn btn-sm btn-success" href="{{ route('tags.create') }}">Excel</a>
            <a class="btn btn-sm btn-success" href="{{ route('tags.create') }}">Pdf</a>
        </div>
        <div class="card-body">

            <x-backend.layouts.elements.message :message="session('message')" />

            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>SL#</th>
                        <th>Title</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $sl = 0;
                    @endphp
                    @foreach ($tag as $tag)
                        <tr>
                            <td>{{ ++$sl }}</td>
                            <td>{{ $tag->title }}</td>
                            <td>
                                <a class="btn btn-info btn-sm" href="{{ route('tags.restore', ['tag'=>$tag->id]) }}" >Restore</a>
    
                                <a class="btn btn-danger btn-sm" href="{{ route('tags.permanentdelete', ['tag'=>$tag->id]) }}" >Permanent Delete</a>
    
                                <!-- <a href="{{ route('tags.destroy', ['tag' => $tag->id]) }}" >Delete</a> -->
    
    
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</x-backend.layouts.master>