<x-backend.layouts.master>
    <x-slot name="pageTitle">
        Edit Form
    </x-slot>

    <x-slot name='breadCrumb'>
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader"> Tags </x-slot>

            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">Edit</li>

        </x-backend.layouts.elements.breadcrumb>
    </x-slot>


    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Edit Tags <a class="btn btn-sm btn-info" href="{{ route('tags.index') }}">List</a>
        </div>
        <div class="card-body">

            <x-backend.layouts.elements.errors :errors="$errors"/>

            <form action="{{ route('tags.update', ['tag' => $tag->id]) }}" method="post"  enctype="multipart/form-data">
                @csrf
                @method('patch')

                <x-backend.form.input name="title" :value="old('title') ?? $tag->title"/>
                <x-backend.form.input name="image" type="file"/>

                <x-backend.form.button>Save</x-backend.form.button>
            </form>
        </div>
    </div>


</x-backend.layouts.master>