<?php

namespace App\Http\Controllers;

use App\Exports\TagExport;
use App\Http\Requests\TagRequest;
use App\Models\Tag;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\DocBlock\Tag as DocBlockTag;
use Image;
use PDF;
use Excel;
use Rap2hpoutre\FastExcel\FastExcel;

class TagController extends Controller
{
    public function index()
    {
        try{
            $tag = Tag::latest()->get();
            return view('backend.tags.index', [
            'tag' => $tag
        ]);
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function create()
    {
        return view('backend.tags.create');
    }

    public function store(TagRequest $request)
    {
        try{
            Tag::create([
                'title' => $request->title,
                'image' => $this->uploadImage(request()->file('image'))
            ]);
            return redirect()->route('tags.index')->withMessage('Task was successful Created!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show(Tag $tag)
    {
        return view('backend.tags.show',[
            'tag' => $tag
        ]);
    }

    public function edit(Tag $tag)
    {
        return view('backend.tags.edit',[
            'tag' =>$tag
        ]);
    }

    public function update(TagRequest $request ,Tag $tag)
    {
        try{

            $requestData = [
                'title' => $request->title
            ];

            if($request->hasFile('image')){
                $requestData['image'] =$this->uploadImage(request()->file('image'));
            }

            $tag->update($requestData);
            return redirect()->route('tags.index')->withMessage('Task was successfully Update!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function destroy(Tag $tag)
    {
        try{
            $tag->delete();
            return redirect()->route('tags.index')->withMessage('Task was successfully Delete!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function trash()
    {
        try{
            $tag = Tag::onlyTrashed()->latest()->get();
            return view('backend.tags.trashed',[
                'tag' => $tag
            ]);
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function restore($id)
    {
        try{
            $tag = Tag::onlyTrashed()->findOrFail($id);
            $tag-> restore();
            return redirect()->route('tags.trash')->withMessage('Task was successfully Restore!');
        }catch(QueryException$e){
            echo $e->getMessage();
        }
        
    }

    public function permanentdelete($id)
    {
        try{
            $tag = Tag::onlyTrashed()->findOrFail($id);
            $tag-> forceDelete();
            return redirect()->route('tags.trash')->withMessage('Task was successfully permanent delete!'); 
        }catch(QueryException$e){
            echo $e->getMessage();
        }
    }

    public function uploadImage($file)
    {
        $fileName = time().'.'.$file->getClientOriginalExtension();
        Image::make($file)->resize(200, 200)->save(storage_path().'/app/public/images/'.$fileName);
        return $fileName;
    }

    public function pdf()
    {
        $tags =Tag::all();
        $pdf = pdf::loadView('backend.tags.indexpdf', ['tags' => $tags]);
        return $pdf->download('tags.indexpdf');
    }

    public function excel()
    {
        return Excel::download(new TagExport, 'tag.xlsx');
    }
}
