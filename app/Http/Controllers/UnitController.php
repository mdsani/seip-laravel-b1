<?php

namespace App\Http\Controllers;

use App\Http\Requests\UnitRequest;
use App\Models\Unit;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    public function index()
    {
        try{
            $unit = Unit::latest()->get();
            return view('backend.units.index', [
            'unit' => $unit
        ]);
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function create()
    {
        return view('backend.units.create');
    }

    public function store(UnitRequest $request)
    {
        try{
            Unit::create([
                'title' => $request->title
            ]);
            return redirect()->route('units.index')->withMessage('Task was successful Created!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show(Unit $unit)
    {
        return view('backend.units.show',[
            'unit' => $unit
        ]);
    }

    public function edit(Unit $unit)
    {
        return view('backend.units.edit',[
            'unit' => $unit
        ]);
    }

    public function update(UnitRequest $request ,Unit $unit)
    {
        try{
            $unit->update([
                'title' => $request->title
            ]);
            return redirect()->route('units.index')->withMessage('Task was successfully Update!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function destroy(Unit $unit)
    {
        try{
            $unit->delete();
            return redirect()->route('tags.index')->withMessage('Task was successfully Delete!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
}
