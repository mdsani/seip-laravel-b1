<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $categoryId = $this->route('category')->id ?? '';
        
        $imageValidationRule = 'image|mimes:png,jpg,jpeg,gif|dimensions:min_width=100,min_height=200|max:1000';

        if($this->isMethod('post')){
            $imageValidationRule = 'required|'.$imageValidationRule;
        }

        return [
            'title' => 'required|min:3|max:50|unique:categories,title,'.$categoryId,
            'description' => ['required', 'min:10'],
            'image' => $imageValidationRule
        ];
    }
}
