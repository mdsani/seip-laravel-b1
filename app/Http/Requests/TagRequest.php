<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $tagId = $this->route('tag')->id ?? '';

        $imageValidation = 'image|mimes:png,jpg|dimensions:min_width=100,min_height=200|max:1000';

        if($this->isMethod('post')){
            $imageValidation ='required|'.$imageValidation;
        }

        return [
            'title' => 'required||unique:tags,title,'.$tagId,
            'image' => $imageValidation
        ];
    }
}
